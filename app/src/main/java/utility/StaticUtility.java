package utility;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Base64;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class StaticUtility extends Application {

    public static final int LoginMaxLen = 19;
    public static final int LoginMinLen = 6;
    public static final int PassMaxLen = 18;
    public static final int PassMinLen = 8;

    //==================================================Variables================================================
    public enum ErrorType {All, NoError, CharsLength, Regex, Empty, Null}
    public enum FieldType {Login, Pass, RptPass, Null}
    public enum FileType {Picture, Video, Sound, Document, Pdf, Archive, File}
    public enum FuncType {read, write}
    public enum MostUsefull {create,reCreate,show,hide,destroy,add,remove}

    //==================================================finals===================================================
    public static interface RequestCode {int CamPic= 0;int CamMov=1;int IntPic=2;int IntFile=3;int ExtPic=4;int ExtFile=5;int Pic=6;int Fil=7;};
    //==================================================finals===================================================
    private static final String patternName = String.format("^[a-zA-Z]+(?=.*[0-9]*)(?=.*[a-zA-Z]+)(?=\\S+$).{%s,%s}$", LoginMinLen, LoginMaxLen);
    private static final String patternPass = String.format("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[\\!\\@\\#\\$%%\\^&\\*\\(\\)_\\+\\-\\=\\[\\]\\{\\}\\|;':\\\",\\.\\/\\<\\>\\?`~])(?=\\S+$).{%s,%s}$", PassMinLen, PassMaxLen);

    //==================================================functions================================================
    public static ErrorType fNullCheck(EditText editText, FieldType fType) {
        if (editText.getText().toString().trim().equals("")) return ErrorType.Empty;
        else return ErrorType.NoError;
    }

    public static ErrorType fMatchPattern(FieldType fType, EditText editText) {
        if (fType == FieldType.Login) {
            if (editText.getText().toString().matches(patternName)) return ErrorType.NoError;
            else return ErrorType.Regex;
        } else if (new HashSet<FieldType>(Arrays.asList(FieldType.Pass, FieldType.RptPass)).contains(fType)) {
            if (editText.getText().toString().matches(patternPass)) return ErrorType.NoError;
            else return ErrorType.Regex;
        }
        return ErrorType.Null;
    }

    public static void hide_keyboard(Context context, View view) {
        InputMethodManager mgr = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static HashMap<FieldType, ErrorType> fErrorCheck(final FieldType fType, final ErrorType erTyp, @NotNull EditText editText) {
        if (new HashSet<ErrorType>(Arrays.asList(ErrorType.Empty, ErrorType.All)).contains(erTyp)) {
            final ErrorType tmpErType = fNullCheck(editText, fType);
            if (tmpErType != ErrorType.NoError) {
                return new HashMap<FieldType, ErrorType>() {{
                    put(fType, tmpErType);
                }};
            } else if (erTyp != ErrorType.All) return new HashMap<FieldType, ErrorType>() {{
                put(fType, ErrorType.NoError);
            }};
        }
        String sString = editText.getText().toString();
        int sLength = sString.length();
        if (new HashSet<ErrorType>(Arrays.asList(ErrorType.CharsLength, ErrorType.All)).contains(erTyp)) {
            if (fType == FieldType.Login) {
                if (sLength > LoginMaxLen || sLength < LoginMinLen) {
                    return new HashMap<FieldType, ErrorType>() {{
                        put(fType, ErrorType.CharsLength);
                    }};
                } else if (erTyp != ErrorType.All) return new HashMap<FieldType, ErrorType>() {{
                    put(fType, ErrorType.NoError);
                }};
            } else if (new HashSet<FieldType>(Arrays.asList(FieldType.Pass, FieldType.RptPass)).contains(fType)) {
                if (sLength > PassMaxLen || sLength < PassMinLen) {
                    return new HashMap<FieldType, ErrorType>() {{
                        put(fType, ErrorType.CharsLength);
                    }};
                } else if (erTyp != ErrorType.All) return new HashMap<FieldType, ErrorType>() {{
                    put(fType, ErrorType.NoError);
                }};
            }
        }
        if (new HashSet<ErrorType>(Arrays.asList(ErrorType.All, ErrorType.Regex)).contains(erTyp)) {
            if (fMatchPattern(fType, editText) != ErrorType.NoError) {
                return new HashMap<FieldType, ErrorType>() {
                    {
                        put(fType, ErrorType.Regex);
                    }
                };
            } else if (erTyp != ErrorType.All) return new HashMap<FieldType, ErrorType>() {{
                put(fType, ErrorType.NoError);
            }};
        }
        return new HashMap<FieldType, ErrorType>() {{
            put(fType, ErrorType.NoError);
        }};
    }

    public static boolean checkPassMatching(TextView edtPass, TextView edtRptPass) {
        if (edtPass.getText().toString().equals(edtRptPass.getText().toString())) {
            return false;
        }
        return true;
    }


    public static String getMd5(String input) {
        try {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }

        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public static Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }
    public abstract static class OnDoubleClickListener implements View.OnClickListener {
        private final int doubleClickTimeout;
        private Handler handler;

        private long firstClickTime;

        public OnDoubleClickListener() {
            doubleClickTimeout = ViewConfiguration.getDoubleTapTimeout();
            firstClickTime = 0L;
            handler = new Handler(Looper.getMainLooper());
        }

        @Override
        public void onClick(final View v) {
            long now = SystemClock.elapsedRealtime();

            if (now - firstClickTime < doubleClickTimeout) {
                handler.removeCallbacksAndMessages(null);
                firstClickTime = 0L;
                onDoubleClick(v);
            } else {
                firstClickTime = now;
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        onSingleClick(v);
                        firstClickTime = 0L;
                    }
                }, doubleClickTimeout);
            }
        }

        public abstract void onDoubleClick(View v);

        public abstract void onSingleClick(View v);

        public void reset() {
            handler.removeCallbacksAndMessages(null);
        }
    }
    public static byte[] Bitmap2ByteArr(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        return stream.toByteArray();
    }
    public static Bitmap ByteArr2Bitmap(byte[] bitmap) {
        return BitmapFactory.decodeByteArray(bitmap, 0, bitmap.length);
    }
    // to use this
    /*



     */
    public static Bitmap getImgVwPic(ImageView imageView) {
        return ((BitmapDrawable)imageView.getDrawable()).getBitmap();
    }

    public static class ListenerEvent { //the is clear, this for notification on Thread Events
        public Context context;boolean bChanged;String strChanged; onValueChangeListener valueChangeListener;Object object= null;
        public ListenerEvent(Context context) { this.context = context; this.bChanged = false; this.strChanged = ""; }
        public ListenerEvent() { this.context = null; this.bChanged = false; this.strChanged = ""; }
        public void setValueChangeListener(onValueChangeListener valueChangeListener) { this.valueChangeListener = valueChangeListener; }
        public onValueChangeListener getValueChangeListener() { return valueChangeListener; }
        public boolean isChanged() { return bChanged; }
        public boolean isStrChanged() { return strChanged != ""; }
        public void setStrChanged(String strIn){
            strChanged = strIn;
            if (valueChangeListener != null) {
                valueChangeListener.onChange();
            }
        }
        public void setObject(Object object){
            this.object = object;
            if (valueChangeListener != null) {
                valueChangeListener.onChange();
            }
        }
        public Object getObject() {
            return object;
        }
        public String getStrChanged(){ return strChanged; }
        public void setBoolVal(boolean value) {
            bChanged = value;
            if (valueChangeListener != null) {
                valueChangeListener.onChange();
//                            bChanged = false;
            }
        }
        public interface onValueChangeListener {
            void onChange();
        }
    }
    public static void copyImageView2another(ImageView imageView1,ImageView imageView2){
        BitmapDrawable drawable = (BitmapDrawable) imageView1.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        imageView2.setImageBitmap(bmp);
    }
    //=====================================================================================================================================
}