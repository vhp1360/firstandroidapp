package utility;
//https://github.com/ECLaboratorio/BubbleShowCase-Android

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.elconfidencial.bubbleshowcase.BubbleShowCase;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseListener;
import com.elconfidencial.bubbleshowcase.BubbleShowCaseSequence;

import java.util.ArrayList;
import java.util.List;

public class BubbleGuide {
    List<BubbleShowCaseBuilder> bubbleShowCaseBuilders;
    public BubbleGuide(){
        this.bubbleShowCaseBuilders= new ArrayList<BubbleShowCaseBuilder>();
    }
    public Boolean fAdd2List(BubbleShowCaseBuilder bubbleShowCaseBuilder){
        try {
            this.bubbleShowCaseBuilders.add(bubbleShowCaseBuilder);
            return true;
        } catch (Exception e){
            return false;
        }
    }
    public void fDefault(Activity activity, String title, String desc, View view){
        new BubbleShowCaseBuilder(activity).title(title).description(desc).targetView(view).show();
    }
    public BubbleShowCaseBuilder fDefaultR(Activity activity, String title, String desc, View view){
        return new BubbleShowCaseBuilder(activity).title(title).description(desc).targetView(view);
    }
    public void fColor(Activity activity, String title, String desc, int bColor,int tColor,View view) {
        new BubbleShowCaseBuilder(activity).title(title).description(desc).backgroundColor(bColor).textColor(tColor).targetView(view).show();
    }
    public void fSequence(ArrayList<BubbleShowCaseBuilder> bubbleShowCaseBuilders){
        if (bubbleShowCaseBuilders == null) new BubbleShowCaseSequence().addShowCases(this.bubbleShowCaseBuilders).show();
        else new BubbleShowCaseSequence().addShowCases(bubbleShowCaseBuilders).show();
    }
    public void fComplete(Activity activity,String title,String desc,String once,int bColor,int tColor,int tSize,int dSize,Drawable bImage,Drawable cImage,
                          BubbleShowCase.ArrowPosition arrowPosition, View view) {
        new BubbleShowCaseBuilder(activity)
                .title(title)
                .description(desc)
                .arrowPosition(arrowPosition)
                .backgroundColor(bColor)
                .textColor(tColor)
                .titleTextSize(tSize)
                .descriptionTextSize(dSize)
                .image(bImage)
                .closeActionImage(cImage)
                .showOnce(once)
//                .listener(listener(object :BubbleShowCaseListener {
//            override fun onTargetClick(bubbleShowCase: BubbleShowCase) {
//
//            }
//            override fun onCloseActionImageClick(bubbleShowCase: BubbleShowCase) {
//
//            }
//            override fun onBubbleClick(bubbleShowCase: BubbleShowCase) {
//
//            }
//
//            override fun onBackgroundDimClick(bubbleShowCase: BubbleShowCase) {
//
//            }
//        })
                .targetView(view)
                .show();
    }
}
