package utility;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.instagram_app.R;

public class CheckPermissions extends ActivityCompat {
    AppCompatActivity appCompatActivity;
    Context context;
    public CheckPermissions(AppCompatActivity appCompatActivity,Context context){
        this.appCompatActivity = appCompatActivity;
        this.context = context;
    }

    public void fCheckPermission(String permission, int requestCode) {
        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_DENIED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(appCompatActivity, new String[]{permission}, requestCode);
            }
            ;
        } else {
            Toast.makeText(context, context.getString(R.string.permGranted), Toast.LENGTH_SHORT).show();
        }
    }

    public Boolean fDoesPermissionGranted(String permission) {
        if (ContextCompat.checkSelfPermission(context.getApplicationContext(), permission) == PackageManager.PERMISSION_GRANTED)
            return true;
        else return false;
    }

    //    =============== to use it : write below codes in Activity and then Call this method
    //    @override
    //    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    //        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    public Boolean fCustomRequestPermissionsResult(@NonNull String[] permissions, @NonNull int[] grantResults) {
        if (permissions[0].equals(Manifest.permission.CAMERA)) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(appCompatActivity, appCompatActivity.getString(R.string.permCameraAlow), Toast.LENGTH_SHORT).show();
                return true;
            } else {
                Toast.makeText(appCompatActivity, appCompatActivity.getString(R.string.permCameraDeny), Toast.LENGTH_SHORT).show();
                return false;
            }
        } else if (permissions[0].equals(Manifest.permission.INTERNET)) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(appCompatActivity, appCompatActivity.getString(R.string.permNetAlow), Toast.LENGTH_SHORT).show();
                return true;
            } else {
                Toast.makeText(appCompatActivity, appCompatActivity.getString(R.string.permNetDeny), Toast.LENGTH_SHORT).show();
                return false;
            }
        } else if (permissions[0].equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(appCompatActivity, appCompatActivity.getString(R.string.permReadStorageAlow), Toast.LENGTH_SHORT).show();
                return true;
            } else {
                Toast.makeText(appCompatActivity, appCompatActivity.getString(R.string.permReadStorageDeny), Toast.LENGTH_SHORT).show();
                return false;
            }
        } else if (permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE))
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(appCompatActivity, appCompatActivity.getString(R.string.permWriteStorageAlow), Toast.LENGTH_SHORT).show();
                return true;
            } else {
                Toast.makeText(appCompatActivity, appCompatActivity.getString(R.string.permWriteStorageDeny), Toast.LENGTH_SHORT).show();
                return false;
            }
        return null;
    }
}
