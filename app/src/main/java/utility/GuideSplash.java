package utility;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.elconfidencial.bubbleshowcase.BubbleShowCaseBuilder;

import java.util.ArrayList;

public class GuideSplash {
//    SquareGuide squareGuide;
    BubbleGuide bubbleGuide;

    public enum GuideEnum { square,bubble,showcase};

    public GuideSplash(GuideEnum guideEnum) {
        if (guideEnum == GuideEnum.bubble) this.bubbleGuide = new BubbleGuide();
//        if (guideEnum == GuideEnum.square) this.squareGuide = new SquareGuide();

    }
//    public Boolean fSquareDefault(Context context, String title, String desc, View view) {
//        return squareGuide.fDefault(context,title,desc,view);
//    }
    public void fBubbleDefault(Activity activity, String title, String desc, View view){
        bubbleGuide.fDefault(activity,title,desc,view);
    }
    public BubbleShowCaseBuilder fBubbleDefaultR(Activity activity, String title, String desc, View view){
        return  bubbleGuide.fDefaultR(activity,title,desc,view);
    }
    public void fColorBubble(Activity activity, String title, String desc, int bColor,int tColor,View view) {
        bubbleGuide.fColor(activity,title,desc,bColor,tColor,view);
    }
    public void fSequenceBubble(ArrayList<BubbleShowCaseBuilder> bubbleShowCaseBuilders) {
        bubbleGuide.fSequence(bubbleShowCaseBuilders);
    }
    public Boolean fAdd2Bubble(BubbleShowCaseBuilder bubbleShowCaseBuilder) {
        return  bubbleGuide.fAdd2List(bubbleShowCaseBuilder);
    }
}
