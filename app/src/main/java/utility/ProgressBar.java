package utility;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;

import com.agrawalsuneet.squareloaderspack.loaders.UsainBoltLoader;
import com.instagram_app.R;

public class ProgressBar {
    public static class RunnerProgress{
        public boolean isProgressRun;
        UsainBoltLoader usainBoltLoader;
        LinearLayout llProgress;
        public RunnerProgress(Context context,ConstraintLayout constraintLayout,int guidelineTop,int guidelineBottom,int color){
            llProgress= new LinearLayout(context);
            ConstraintSet constraintSet = new ConstraintSet();
            usainBoltLoader = new UsainBoltLoader(context,60, ContextCompat.getColor(context, color));
            constraintSet.clone(constraintLayout);
            constraintSet.connect(llProgress.getId(),ConstraintSet.TOP,guidelineTop,ConstraintSet.TOP);
            constraintSet.connect(llProgress.getId(),ConstraintSet.BOTTOM,guidelineBottom,ConstraintSet.BOTTOM);
            constraintSet.connect(llProgress.getId(),ConstraintSet.START,constraintLayout.getId(),ConstraintSet.START);
            constraintSet.connect(llProgress.getId(),ConstraintSet.END,constraintLayout.getId(),ConstraintSet.END);
            usainBoltLoader.setGravity(Gravity.CENTER);
            llProgress.setGravity(Gravity.CENTER);
            llProgress.addView(usainBoltLoader);
            llProgress.requestLayout();
            constraintSet.applyTo(constraintLayout);
            constraintLayout.addView(llProgress);
            isProgressRun=false;
        }
        public void fInterWindProgress(StaticUtility.MostUsefull typeIn){
            switch (typeIn) {
                case create:
                    break;
                case show:
                    if (llProgress == null || isProgressRun) break;
                    llProgress.setVisibility(View.VISIBLE);
                    isProgressRun=true;
                    break;
                case hide:
                    if (llProgress!= null) {
                        llProgress.setVisibility(View.INVISIBLE);
                    }
                    isProgressRun= false;
                    break;
            }

        }

    }
}
