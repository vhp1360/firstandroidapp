package utility;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import utility.CheckPermissions;

public class ActiveUtility extends AppCompatActivity {
    public  CheckPermissions checkPermissions;
    public Bitmap fromOnActivityResult;
    ContentResolver contentResolver;
    Context context;
    AppCompatActivity appCompatActivity;
    SharedPreferences sp;
    // How to use it
    /*
        private void imgCurrUserDoubleClick(View v) {
            activeUtility.openCamera(this,getApplicationContext());
        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            activeUtility.onActivityResult(requestCode,resultCode,data);
            imgCurrUser.setImageBitmap(activeUtility.fromCameraOpening);
            activeUtility.fromCameraOpening = null;
            newMD5 = getMd5(activeUtility.fromCameraOpening.toString());
            fCheckItemForSave(imgCurrUser);
        }

     */
    public ActiveUtility(AppCompatActivity appCompatActivity, Context context, ContentResolver contentResolver){
        this.contentResolver = contentResolver;
        this.appCompatActivity = appCompatActivity;
        this.context = context;
        this.checkPermissions = new CheckPermissions(appCompatActivity,context);
    }


    public void openCamera() {
        if (checkPermissions.fDoesPermissionGranted(Manifest.permission.CAMERA)) {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            appCompatActivity.startActivityForResult(cameraIntent, StaticUtility.RequestCode.CamPic);

        } else checkPermissions.fCheckPermission(Manifest.permission.CAMERA,StaticUtility.RequestCode.CamPic);

    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void uploadFile(StaticUtility.FileType fileType) {
        if (fileType == StaticUtility.FileType.Picture) {
            if (checkPermissions.fDoesPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Intent imgIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                appCompatActivity.startActivityForResult(imgIntent, StaticUtility.RequestCode.ExtPic);
            } else
                checkPermissions.fCheckPermission(Manifest.permission.READ_EXTERNAL_STORAGE, StaticUtility.RequestCode.ExtPic);
        } else if (fileType == StaticUtility.FileType.File) {

        }
    }

//    @Override
    public void onActivityResults(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == StaticUtility.RequestCode.ExtPic && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = contentResolver.query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            fromOnActivityResult = BitmapFactory.decodeFile(picturePath);
        } else if (requestCode == StaticUtility.RequestCode.CamPic && resultCode == RESULT_OK && data != null) {
            fromOnActivityResult = (Bitmap) data.getExtras().get("data");
        }
    }
    public String sharedpreferencesInfo(StaticUtility.FuncType funcType, String spTypes, String spData) {
        if (sp == null ) sp = context.getSharedPreferences("appSP",MODE_PRIVATE);
        switch (funcType) {
            case read:
                return sp.getString(spTypes,spData);
            case write:
                sp.edit().putString(spTypes,spData).apply();
                return "";
        }
        return null;
    }
}
