package myRecyceler;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;

//import utility.StaticUtility;

public class PicModel { //implements Serializable {
    Bitmap pic;
//    public PicModel(){}
    public PicModel(Bitmap picIn){
        pic = picIn;
    }
    public Bitmap getPic() {
        return pic;
    }

}

//public class PicModel implements Serializable,Parcelable {
//    Bitmap pic;
//    public PicModel(Bitmap picIn){
//        pic = picIn;
//    }
//
//    public PicModel(Parcel parcel) {
//        pic = StringToBitMap(parcel.readString());
//    }
//    public Bitmap getPic() {
//        return pic;
//    }
//
//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeString(BitMapToString(pic));
//    }
//    public static final Creator<PicModel> CREATOR = new Parcelable.Creator<PicModel>() {
//        @Override
//        public PicModel[] newArray(int size) {
//            return new PicModel[size];
//        }
//
//        @Override
//        public PicModel createFromParcel(Parcel source) {
//            return new PicModel(source);
//        }
//    };
//    String BitMapToString(Bitmap bitmap) {
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
//        byte[] b = baos.toByteArray();
//        String temp = Base64.encodeToString(b, Base64.DEFAULT);
//        return temp;
//    }
//
//    Bitmap StringToBitMap(String encodedString) {
//        try {
//            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
//            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
//            return bitmap;
//        } catch (Exception e) {
//            e.getMessage();
//            return null;
//        }
//    }
//}
