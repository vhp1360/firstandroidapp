package myRecyceler;

import android.graphics.Bitmap;
import android.media.Image;
import android.widget.ImageView;

public class DataModel {

    public static class CustomListItem {

        String userName;
        String userLastSeen;
        String userNo_ofPost;
        Bitmap userPic;

        public CustomListItem(String userName, String userLastSeen, String userNo_ofPost, Bitmap userPic) {
            this.userName = userName;
            this.userLastSeen = userLastSeen;
            this.userNo_ofPost = userNo_ofPost;
            this.userPic = userPic;
        }
        public String getUserName() {
            return userName;
        }
        public String getUserLastSeen() {
            return userLastSeen;
        }
        public String getUserNo_ofPost() {
            return userNo_ofPost;
        }
        public Bitmap getUserPic() {
            return userPic;
        }
    }
}