package myRecyceler;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.instagram_app.R;

public class InstaPicHolder extends RecyclerView.ViewHolder {

    ImageView picImg;
    public InstaPicHolder(@NonNull View itemView) {
        super(itemView);
        picImg = itemView.findViewById(R.id.picImg);

    }
    public void fillData(PicModel picModel){
        picImg.setImageBitmap(picModel.getPic());
    }
}
