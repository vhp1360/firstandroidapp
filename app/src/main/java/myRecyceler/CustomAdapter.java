package myRecyceler;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.instagram_app.R;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<DataModel.CustomListItem> implements View.OnClickListener{

    public  boolean isImage;
    private ArrayList<DataModel.CustomListItem> dataSet;
    Context mContext;
    public class ViewHolder {
        TextView txtUserName;
        TextView txtUserLastSeen;
        TextView txtUserNo_ofPost;
        ImageView imgUserPic;
        ImageView imgArrow;
    }

    public CustomAdapter(ArrayList<DataModel.CustomListItem> data, Context context) {
        super(context, R.layout.list_row, data);
        this.dataSet = data;
        this.mContext=context;
    }

    @Override
    public void onClick(View v) {
        isImage = v.getId() == R.id.imgArrow;
        int position=(Integer) v.getTag();
        Object object= getItem(position);
        DataModel.CustomListItem dataModel=(DataModel.CustomListItem)object;
        switch (v.getId())
        {
            case R.id.imgArrow:
                DataModel.CustomListItem customListItem = dataModel;
                break;
        }
    }

    private int lastPosition = -1;
//    public ImageView getImageView(int position,View view){
//        return getView(position,view,null).imgeView()
//    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DataModel.CustomListItem dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_row, parent, false);
            viewHolder.txtUserName = (TextView) convertView.findViewById(R.id.txtUserName);
            viewHolder.txtUserLastSeen = (TextView) convertView.findViewById(R.id.txtUserLastSeen);
            viewHolder.txtUserNo_ofPost= (TextView) convertView.findViewById(R.id.txtUserNo_ofPost);
            viewHolder.imgUserPic = (ImageView) convertView.findViewById(R.id.imgUserPic);
            viewHolder.imgArrow = (ImageView) convertView.findViewById(R.id.imgArrow);

            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtUserName.setText(dataModel.getUserName());
        viewHolder.txtUserLastSeen.setText(dataModel.getUserLastSeen());
        viewHolder.txtUserNo_ofPost.setText(dataModel.getUserNo_ofPost());
        viewHolder.imgUserPic.setImageBitmap(dataModel.getUserPic());
        viewHolder.imgArrow.setOnClickListener(this);
        viewHolder.imgArrow.setTag(position);
        return convertView;  // Return the completed view to render on screen
    }
}