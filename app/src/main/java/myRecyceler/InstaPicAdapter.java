package myRecyceler;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.instagram_app.R;

import java.util.ArrayList;

public class InstaPicAdapter extends RecyclerView.Adapter<InstaPicHolder> implements getItemNo {
    Context context;
    ArrayList<PicModel> picModels;
    ItemOnClick itemOnClick;
    Integer position;

    public InstaPicAdapter(Context context1,ArrayList<PicModel> picModels1,ItemOnClick itemOnClick1){
        context=context1;
        picModels=picModels1;
        itemOnClick = itemOnClick1;
        position = null;
    }

    @NonNull
    @Override
    public InstaPicHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.pic_row,parent,false);
        final InstaPicHolder instaPicHolder = new InstaPicHolder(view);
        instaPicHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemOnClick.onItemClick(v); //,instaPicHolder.getAdapterPosition());
            }
        });
        return instaPicHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull InstaPicHolder holder, int position) {
        PicModel picModel=picModels.get(position);
        holder.fillData(picModel);
        this.position = position;
    }

    @Override
    public int getItemCount() {
        return picModels.size();
    }
    public interface ItemOnClick{
        void onItemClick(View view); //,int position);
    }
}
