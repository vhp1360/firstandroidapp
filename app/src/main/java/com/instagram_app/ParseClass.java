package com.instagram_app;

import com.parse.FindCallback;
import com.parse.Parse;
import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.widget.Toast;

import utility.StaticUtility.*;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static utility.StaticUtility.Bitmap2ByteArr;

public class ParseClass extends Application {
        ParseFile parseFile;
        ParseObject parseObject;
        private ListenerEvent listenerEvent= null;

        @Override
        public void onCreate() {
                super.onCreate();
                Parse.setLogLevel(Parse.LOG_LEVEL_DEBUG);
                Parse.initialize(new Parse.Configuration.Builder(this)
                        .applicationId(getString(R.string.appID)) // should correspond to Application Id env variable
                        .clientKey(getString(R.string.cleintKey))  // should correspond to Client key env variable
                        .server(getString(R.string.serverAddr)).enableLocalDataStore().build());
        }
        public String fSendInstaFile2Parse(final Context context, Bitmap bitmap, String fileName) {
                return fSendFile2Parse(context,bitmap,fileName,"InstaImages");
        }
        public String fSendFile2Parse(final Context context, Bitmap bitmap, String fileName, final String className) {
                if (listenerEvent == null) listenerEvent  = new ListenerEvent(context);
                listenerEvent.setValueChangeListener(new ListenerEvent.onValueChangeListener() {
                        @Override
                        public void onChange() {
                                if (listenerEvent.isStrChanged())
                                        Toast.makeText(listenerEvent.context, listenerEvent.getStrChanged(), Toast.LENGTH_SHORT).show();
                                else
                                        Toast.makeText(listenerEvent.context, "Nothing", Toast.LENGTH_SHORT).show();
                        }
                });
                final String[] result = new String[1];
                parseFile = new ParseFile(fileName,Bitmap2ByteArr(bitmap));
                parseFile.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                                if (e == null) {
                                        parseObject = new ParseObject(className);
                                        parseObject.put("username", ParseUser.getCurrentUser().getUsername().toString());
                                        parseObject.put("img",parseFile);
                                        parseObject.saveInBackground(new SaveCallback() {
                                                @Override
                                                public void done(ParseException e) {
                                                        if (e == null) {
                                                                listenerEvent.setStrChanged(context.getString(R.string.imgSaved));
                                                        } else {
                                                                listenerEvent.setStrChanged(context.getString(R.string.imgErrInObj));
                                                        }
                                                }
                                        });
                                } else {
                                        listenerEvent.setStrChanged(context.getString(R.string.imgErrInImg));
                                }
                        }
                });
//                return result[0];
                Toast.makeText(context, "This is Test", Toast.LENGTH_SHORT).show();
            return listenerEvent.getStrChanged();
        }
        public void ParseQueries(String strInventoryName, HashMap<String,String> hashWhere){
                ParseQuery<ParseObject> query = ParseQuery.getQuery(strInventoryName);
                for (Map.Entry<String,String> entry: hashWhere.entrySet()) {
                        query.whereEqualTo(entry.getKey(), entry.getValue());
                }
                query.findInBackground(new FindCallback<ParseObject>() {
                        public void done(List<ParseObject> scoreList, ParseException e) {
                                if (e == null) {
                                        Log.d("score", "Retrieved " + scoreList.size() + " scores");
                                } else {
                                        Log.d("score", "Error: " + e.getMessage());
                                }
                        }
                });
        }
        public static void deleteAll(String pinned) throws ParseException {
                ParseObject.unpinAll(pinned);
        }
}
