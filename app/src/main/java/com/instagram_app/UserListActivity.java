package com.instagram_app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import myRecyceler.CustomAdapter;
import myRecyceler.DataModel;
import utility.ActiveUtility;
import utility.GuideSplash;
import utility.StaticUtility;

import static utility.StaticUtility.BitMapToString;
import static utility.StaticUtility.StringToBitMap;
import static utility.StaticUtility.getImgVwPic;
import static utility.StaticUtility.getMd5;

public class UserListActivity extends AppCompatActivity {

    ActiveUtility activeUtility;
    ParseClass parseClass;
    ParseUser currUser;
    ParseFile parseFile;
    ParseObject parseObject;
    TextView txtCurrUser;
    ImageView imgCurrUser;
    ListView listView;
    String oldMD5,newMD5;
    Boolean bUsrCamPic = false;
    Boolean bUploadCam = false;
    Boolean bUploadFile = false;
    Boolean bUsrFilePic = false;
    StaticUtility.ListenerEvent listenerEvent;
    ArrayList<DataModel.CustomListItem> customListItems;
    CustomAdapter customAdapter;
    boolean showGuide = true;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        activeUtility.checkPermissions.fCustomRequestPermissionsResult(permissions, grantResults);
    }

    private void imgCurrUserSingleClick(View view){
        bUsrFilePic =true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activeUtility.uploadFile(StaticUtility.FileType.Picture);
        }

    }
    private void imgCurrUserDoubleClick(View v) {
        bUsrCamPic = true;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activeUtility.openCamera();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) return;
        activeUtility.onActivityResults(requestCode,resultCode,data);
        if (bUsrCamPic || bUsrFilePic) {
            fSetUserPic(activeUtility.fromOnActivityResult);
            activeUtility.fromOnActivityResult = null;
            bUsrCamPic = false;bUsrFilePic = false;
        } else if (bUploadCam || bUploadFile) {
            listenerEvent.setStrChanged(parseClass.fSendInstaFile2Parse(getApplicationContext(),activeUtility.fromOnActivityResult,Long.toString(System.currentTimeMillis())));
            bUploadCam = false;bUploadFile = false;
        }
    }

    public void fCheckItemForSave(View view) {
        boolean changed= false;
        if (view.getId() == imgCurrUser.getId()) {
            if (oldMD5 != newMD5) {
                Bitmap bm=((BitmapDrawable)((ImageView) view).getDrawable()).getBitmap();
                String strBitmap = BitMapToString(bm);
                currUser.put("profilePic", strBitmap);
                changed = true;
            }
        }
        if (changed) {
                currUser.saveInBackground();
        }
    }
    private void fSetUserPic(Bitmap bmIn) {
        oldMD5 = getMd5(getImgVwPic(imgCurrUser).toString());
        if (bmIn != null) {
            imgCurrUser.setImageBitmap(bmIn);
        }
        else { imgCurrUser.setImageResource(R.drawable.defaultprofile);
        }
        newMD5 = getMd5(getImgVwPic(imgCurrUser).toString());
        fCheckItemForSave(imgCurrUser);
    }
    public void fListenerChangedEvent(){
        if (listenerEvent.getStrChanged() != "") Toast.makeText(this, listenerEvent.getStrChanged(), Toast.LENGTH_SHORT).show();
    }
    private void fStartUpInit(){
        fUserInfo();
        fUsersInfo();
    }
    private void fUserInfo(){
        currUser = ParseUser.getCurrentUser();
        if (currUser != null) {
            txtCurrUser = (TextView)findViewById(R.id.txtCurrUser);
            imgCurrUser = (ImageView) findViewById(R.id.imgCurrUser);
            txtCurrUser.setText(currUser.getUsername());
            Bitmap bm = StringToBitMap((String)currUser.get("profilePic"));
            fSetUserPic(bm);

        } else {
            // show the signup or login screen
        }
        imgCurrUser.setOnClickListener(new StaticUtility.OnDoubleClickListener() {
            @Override
            public void onDoubleClick(View v) {
                singleClick(v);
            }

            @Override
            public void onSingleClick(View v) {
                doubleClick(v);
            }
        });
    }
    private void fUsersInfo(){
        if (currUser == null) return;
        ParseQuery<ParseObject> qUser = ParseQuery.getQuery("_User");
        qUser.whereNotEqualTo("username",currUser.getUsername());
        qUser.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> instaUsers, ParseException e) {
                if (e == null) {
                    customListItems = new ArrayList<>();
                    for (int i=0;i<instaUsers.size();i++){
                        customListItems.add(new DataModel.CustomListItem(instaUsers.get(i).getString("username"),instaUsers.get(i).getString("updatedAt"),"10",StringToBitMap(instaUsers.get(i).getString("profilePic"))));
                    }
                    customAdapter = new CustomAdapter(customListItems,getApplicationContext());
                    listView = (ListView)findViewById(R.id.usrlstUsers);
                    listView.setAdapter(customAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            DataModel.CustomListItem customListItem = customListItems.get(position);
                            fGo2UserPosts(customListItem.getUserName());
                        }
                    });
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }
    private void fGo2UserPosts(String userName){
        Intent usrPostsIntent = new Intent(this,UserPosts.class);
        usrPostsIntent.putExtra("userName",userName);
        startActivity(usrPostsIntent);

    }
    private void fUsrLstGuide() {
        if (! showGuide) return;
        GuideSplash guideSplash = new GuideSplash(GuideSplash.GuideEnum.bubble);
        guideSplash.fAdd2Bubble(guideSplash.fBubbleDefaultR(this,getString(R.string.strProfoPic),getString(R.string.strProfoPicDesc),imgCurrUser));
        guideSplash.fAdd2Bubble(guideSplash.fBubbleDefaultR(this,getString(R.string.strCurrUser),getString(R.string.strCurrUserDesc),txtCurrUser));
        guideSplash.fSequenceBubble(null);
        showGuide = false;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        activeUtility = new ActiveUtility(this,getApplicationContext(),getContentResolver());
        fStartUpInit();
        listenerEvent = new StaticUtility.ListenerEvent(getApplicationContext());
        listenerEvent.setValueChangeListener(new StaticUtility.ListenerEvent.onValueChangeListener() {
            @Override
            public void onChange() {
                if (listenerEvent.isStrChanged()) fListenerChangedEvent();
            }
        });
        registerForContextMenu(txtCurrUser); // this is for adding Menu
        fUsrLstGuide();
        parseClass = new ParseClass();
    }
    private void singleClick(View v) {
        if (v.getId() == imgCurrUser.getId()) imgCurrUserSingleClick(v);
    }
    private void doubleClick(View v) {
        if (v.getId() == imgCurrUser.getId()) imgCurrUserDoubleClick(v);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(getString(R.string.rightMenuUserFeatures));
        menu.add(0,0,0,getString(R.string.changeUserInfo));
        menu.add(0,1,0,getString(R.string.defaultPic));
        menu.add(0,2,0,getString(R.string.uploadCamera));
        menu.add(0,3,0,getString(R.string.uploadFMedia));
        menu.add(0,4,0,getString(R.string.uploadFile));
        menu.add(0,5,0,getString(R.string.strMySends));
        menu.add(0,6,0,getString(R.string.strLogout));
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case 0 :
                break;
            case 1 :
                fSetUserPic(null);
                break;
            case 2 :
                bUploadCam = true;
               activeUtility.openCamera();
               break;
            case 3 :
                bUploadFile = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    activeUtility.uploadFile(StaticUtility.FileType.Picture);
                }
                break;
            case 4 :
            case 5 :
                fGo2UserPosts(currUser.getUsername());
                break;
            case 6 :
                fLogOut();
                break;

        }
        return true; //super.onContextItemSelected(item);
    }
    public void fLogOut(){
        activeUtility.sharedpreferencesInfo(StaticUtility.FuncType.write, "LogName", "");
        activeUtility.sharedpreferencesInfo(StaticUtility.FuncType.write, "LogPass", "");
        finish();
    }
}