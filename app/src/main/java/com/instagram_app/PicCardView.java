package com.instagram_app;
//https://github.com/yuyakaido/CardStackView/blob/b803b8ae1e18b38f326a813167a0b24587b3aa32/sample/src/main/java/com/yuyakaido/android/cardstackview/sample/CardStackAdapter.kt
//https://kmshack.github.io/AndroidUICollection/2016/10/12/CardStackView/

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.boltsinternal.Continuation;
import com.parse.boltsinternal.Task;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackView;

import java.util.ArrayList;
import java.util.List;

import myRecyceler.InstaPicAdapter;
import myRecyceler.InstaPicHolder;
import myRecyceler.PicModel;
import utility.StaticUtility;

public class PicCardView extends AppCompatActivity {

    RecyclerView.Adapter<InstaPicHolder> picModelAdapter;
    public ArrayList<PicModel> arrPicModels;
    String userName;
    int picNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pic_card_view);
        userName = getIntent().getStringExtra("userName");
        picNo = getIntent().getIntExtra("ItemNo",1);
        CardStackView cardStackView = (CardStackView)findViewById(R.id.cardStack);
        cardStackView.setLayoutManager(new CardStackLayoutManager(this));
        arrPicModels = new ArrayList<>();
        picModelAdapter = new InstaPicAdapter(getApplicationContext(),arrPicModels,itemOnClick);
        CardStackLayoutManager manager = new CardStackLayoutManager(getApplicationContext());
        cardStackView.setLayoutManager(manager);
        cardStackView.setAdapter(picModelAdapter);
        final ParseQuery queryImgs = ParseQuery.getQuery("InstaImages");
        try {
            queryImgs.fromPin("PinPics").findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> userPosts, ParseException e) {
                    if (e == null && userPosts.size()>0) {
                        for (int i=0;i<userPosts.size();i++) {
                            try {
                                ParseFile parseFile = userPosts.get(i).getParseFile("img");
                                arrPicModels.add(new PicModel(StaticUtility.ByteArr2Bitmap(parseFile.getData())));
                                picModelAdapter.notifyDataSetChanged();
                            } catch (ParseException parseException) {
                                parseException.printStackTrace();
                            }
                        }
                    } else {
    //                    Log.d("score", "Error: " + e.getMessage());
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    final InstaPicAdapter.ItemOnClick itemOnClick = new InstaPicAdapter.ItemOnClick() {
        @Override
        public void onItemClick(View view){ //, int position) {
//            super.;
        }
    };
}