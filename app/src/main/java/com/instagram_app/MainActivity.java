package com.instagram_app;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agrawalsuneet.squareloaderspack.loaders.UsainBoltLoader;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

import utility.ActiveUtility;
import utility.GuideSplash;
import utility.ProgressBar;
import utility.StaticUtility;

public class MainActivity extends AppCompatActivity {

    ActiveUtility activeUtility;
    boolean bLog0Sign= true;
    ConstraintLayout constraintLayout;
    Button btnLog0Sign;
    TextView lblLog0Sign;
    TextInputEditText edtLoginName,edtPass,edtRptPass;
    TextInputLayout lytLoginName,lytPass,lytRptPass;
    LinearLayout loginLnLytHz3;
    ParseUser user = new ParseUser();
    boolean isProgressRun;
    private ParseUser parseUser;
    ProgressBar.RunnerProgress runnerProgress;
    boolean showGuide = true;

    public void lblLog0Sign(View view){
       String strSwap = btnLog0Sign.getText().toString();
       btnLog0Sign.setText(lblLog0Sign.getText().toString());
       lblLog0Sign.setText(strSwap);
       bLog0Sign = ! bLog0Sign ;
       if (loginLnLytHz3.isShown()) {
           loginLnLytHz3.setVisibility(View.INVISIBLE);
           loginLnLytHz3.setEnabled(false);
       } else {
           loginLnLytHz3.setVisibility(View.VISIBLE);
           loginLnLytHz3.setEnabled(true);
       }
       if (runnerProgress != null) runnerProgress.fInterWindProgress(StaticUtility.MostUsefull.hide);
    }
    @NotNull
    public void btnLog0Sign(View view) {
        StaticUtility.hide_keyboard(getApplicationContext(),view);
        if (existsAnyErrors(StaticUtility.FieldType.Login)) return;
        if (existsAnyErrors(StaticUtility.FieldType.Pass)) return;
        if (existsAnyErrors(StaticUtility.FieldType.RptPass)) return;
        fLog0Sign(edtLoginName.getText().toString(),edtPass.getText().toString());
    }
    @SuppressLint("StringFormatMatches")
    private Boolean existsAnyErrors(StaticUtility.FieldType fieldType) {
        if (fieldType == StaticUtility.FieldType.Login) {
            HashMap result = StaticUtility.fErrorCheck(StaticUtility.FieldType.Login, StaticUtility.ErrorType.All, edtLoginName);
            if (result.get(StaticUtility.FieldType.Login) != StaticUtility.ErrorType.NoError) {
                if (result.get(StaticUtility.FieldType.Login) == StaticUtility.ErrorType.Empty) {
                    lytLoginName.setError(getString(R.string.strFieldNullErr));
                } else if (result.get(StaticUtility.FieldType.Login) == StaticUtility.ErrorType.CharsLength) {
                    lytLoginName.setError(getString(R.string.strNameLength, StaticUtility.LoginMinLen, StaticUtility.LoginMaxLen));
                } else if (result.get(StaticUtility.FieldType.Login) == StaticUtility.ErrorType.Regex) {
                    lytLoginName.setError(getString(R.string.strCharsMatchAlphDigit));
                }
                return true;
            } else {
                lytLoginName.setError("");
                return false;
            }
        } else if (fieldType == StaticUtility.FieldType.Pass) {
            HashMap result = StaticUtility.fErrorCheck(StaticUtility.FieldType.Pass, StaticUtility.ErrorType.All, edtPass);
            if (result.get(StaticUtility.FieldType.Pass) != StaticUtility.ErrorType.NoError) {
                if (result.get(StaticUtility.FieldType.Pass) == StaticUtility.ErrorType.Empty) {
                    lytPass.setError(getString(R.string.strFieldNullErr));
                } else if (result.get(StaticUtility.FieldType.Pass) == StaticUtility.ErrorType.CharsLength) {
                    lytPass.setError(getString(R.string.strPassLength, StaticUtility.PassMinLen, StaticUtility.PassMaxLen));
                } else if (result.get(StaticUtility.FieldType.Pass) == StaticUtility.ErrorType.Regex) {
                    lytPass.setError(getString(R.string.strCharsMatchAlphDigitSymb));
                }
                return true;
            } else {
                lytPass.setError("");
                return false;
            }
        } else if (fieldType == StaticUtility.FieldType.RptPass) {
            if (! bLog0Sign) {
                HashMap result = StaticUtility.fErrorCheck(StaticUtility.FieldType.RptPass, StaticUtility.ErrorType.All, edtRptPass);
                if (result.get(StaticUtility.FieldType.RptPass) != StaticUtility.ErrorType.NoError) {
                    if (result.get(StaticUtility.FieldType.RptPass) == StaticUtility.ErrorType.Empty) {
                        lytRptPass.setError(getString(R.string.strFieldNullErr));
                    } else if (result.get(StaticUtility.FieldType.RptPass) == StaticUtility.ErrorType.CharsLength) {
                        lytRptPass.setError(getString(R.string.strPassLength, StaticUtility.PassMinLen, StaticUtility.PassMaxLen));
                    } else if (result.get(StaticUtility.FieldType.RptPass) == StaticUtility.ErrorType.Regex) {
                        lytRptPass.setError(getString(R.string.strCharsMatchAlphDigitSymb));
                    }
                    return true;
                } else if (StaticUtility.checkPassMatching(edtPass,edtRptPass)) {
                    Toast.makeText(this, getString(R.string.strPassNotMatching), Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    lytRptPass.setError("");
                    return false;
                }
            } else return false;
        }
        return null;
    }

    @NotNull
    private void fGo2UsersListInflate(String logName,String logPass){
        String oldLogName=activeUtility.sharedpreferencesInfo(StaticUtility.FuncType.read, "LogName", "");
        if (logName != "" && oldLogName != logName) {
            activeUtility.sharedpreferencesInfo(StaticUtility.FuncType.write, "LogName", logName);
            activeUtility.sharedpreferencesInfo(StaticUtility.FuncType.write, "LogPass", logPass);
        }
        Intent usrLstIntent = new Intent(this,UserListActivity.class);
        if (runnerProgress != null) runnerProgress.fInterWindProgress(StaticUtility.MostUsefull.hide);
        startActivity(usrLstIntent);
    }
    private void fLog0Sign(final String logName, final String logPass){
        Guideline guidelineTop = (Guideline)findViewById(R.id.guideline2);
        Guideline guidelineBottom = (Guideline)findViewById(R.id.guideline6);
        if (runnerProgress == null) runnerProgress = new ProgressBar.RunnerProgress(this,constraintLayout,guidelineTop.getId(),guidelineBottom.getId(),R.color.blue_default);
        runnerProgress.fInterWindProgress(StaticUtility.MostUsefull.show);
        parseUser.setUsername(logName);
        parseUser.setPassword(logPass);
        if (bLog0Sign) {
            ParseUser.logInInBackground(logName,logPass,new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {
                    if (user != null) {
                        fGo2UsersListInflate(logName,logPass);
                    } else {
                        ParseUser.logOut();
                        Toast.makeText(MainActivity.this, getString(R.string.strWrongLg0Pass), Toast.LENGTH_SHORT).show();
                        if (runnerProgress != null) runnerProgress.fInterWindProgress(StaticUtility.MostUsefull.hide);
                    }
                }
            });
        } else {
            final boolean[] bIsExistUser = new boolean[1];
    //        user.put("phone", "650-253-0000");
            ParseQuery<ParseObject> queryUsers = ParseQuery.getQuery("User");
            queryUsers.whereEqualTo("username", parseUser.getUsername().toString());
            queryUsers.findInBackground(new FindCallback<ParseObject>() {
                                            public void done(List<ParseObject> results, ParseException e) {
                                                if (e == null) {
                                                    if (results.size() > 0) {
                                                        bIsExistUser[0] = true;
                                                    } else {
                                                        bIsExistUser[0] = false;
                                                    }
                                                } else {
                                                    Toast.makeText(MainActivity.this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                                                    if (runnerProgress != null) runnerProgress.fInterWindProgress(StaticUtility.MostUsefull.hide);
                                                }
                                            }
                                        });
            if (! bIsExistUser[0]) {
                parseUser.signUpInBackground(new SignUpCallback() {

                    @Override
                    public void done(com.parse.ParseException e) {
                        if (e == null) {
                            fGo2UsersListInflate(logName,logPass);
                        } else {
                            Toast.makeText(MainActivity.this, getString(R.string.strSignUpError), Toast.LENGTH_SHORT).show();
                            edtLoginName.requestFocus();
                            if (runnerProgress != null) runnerProgress.fInterWindProgress(StaticUtility.MostUsefull.hide);
                        }
                    }
                });
            }
        }
    }

    @NotNull
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onImageClick(View view){
        StaticUtility.hide_keyboard(getApplicationContext(),view);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activeUtility = new ActiveUtility(this,getApplicationContext(),getContentResolver());
        String logName,logPass;
        btnLog0Sign = (Button)findViewById(R.id.btnLog0Sign);
        lblLog0Sign = (TextView) findViewById(R.id.lblLog0Sign);
        loginLnLytHz3 = (LinearLayout)findViewById(R.id.loginLnLytHr3);
        edtLoginName = (TextInputEditText)findViewById(R.id.loginEdtLogin);
        edtPass = (TextInputEditText)findViewById(R.id.loginEdtPass);
        edtRptPass = (TextInputEditText)findViewById(R.id.loginEdtRptPass);
        lytLoginName = (TextInputLayout)findViewById(R.id.etLoginNameLayout);
        lytPass = (TextInputLayout)findViewById(R.id.etPasswordLayout);
        lytRptPass = (TextInputLayout)findViewById(R.id.etRptPasswordLayout);
        constraintLayout = (ConstraintLayout)findViewById(R.id.constrainLayout);
        parseUser = new ParseUser();
        fMainAppGuid();
        logName=activeUtility.sharedpreferencesInfo(StaticUtility.FuncType.read,"LogName","");
        if (logName != "") {
            logPass = activeUtility.sharedpreferencesInfo(StaticUtility.FuncType.read, "LogPass", "");
            fLog0Sign(logName, logPass);
            return;
        }
        edtLoginName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void afterTextChanged(Editable s) {
                existsAnyErrors(StaticUtility.FieldType.Login);
            }
        });
        edtPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void afterTextChanged(Editable s) {
                existsAnyErrors(StaticUtility.FieldType.Pass);

            }
        });
        edtRptPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void afterTextChanged(Editable s) {
                existsAnyErrors(StaticUtility.FieldType.RptPass);
            }
        });
    }

    private void fMainAppGuid() {
        if (! showGuide) return;;
        GuideSplash guideSplash = new GuideSplash(GuideSplash.GuideEnum.bubble);
        guideSplash.fAdd2Bubble(guideSplash.fBubbleDefaultR(this,getString(R.string.strLogInName),getString(R.string.strLogInNameDesc),edtLoginName));
        guideSplash.fAdd2Bubble(guideSplash.fBubbleDefaultR(this,getString(R.string.strEdtPass),getString(R.string.strEdtPassDesc),edtPass));
        guideSplash.fAdd2Bubble(guideSplash.fBubbleDefaultR(this,getString(R.string.strTextLog0SignDesc),getString(R.string.strTextLog0Sign),lblLog0Sign));
        guideSplash.fSequenceBubble(null);
        showGuide = false;
    }
}