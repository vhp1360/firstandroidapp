package com.instagram_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

import myRecyceler.InstaPicAdapter;
import myRecyceler.InstaPicHolder;
import myRecyceler.PicModel;
import utility.StaticUtility;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;

public class UserPosts extends AppCompatActivity {

    ConstraintLayout constraintLayout;
    String userPosts;
    ConstraintLayout.LayoutParams imageViewParam;
    ConstraintLayout.LayoutParams showImageViewParam;
    ArrayList<PicModel> arrPicModels;
    RecyclerView picRecyclerView;
    RecyclerView.Adapter<InstaPicHolder> picModelAdapter;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_posts);
        context = this;
        if (getIntent().getExtras() == null) return;
        userPosts = (String)getIntent().getExtras().get("userName");
        constraintLayout = (ConstraintLayout)findViewById(R.id.imgInflateLayout);
        arrPicModels = new ArrayList<>();
        picRecyclerView = (RecyclerView) findViewById(R.id.picecyclerView);
        picRecyclerView.setLayoutManager(new GridLayoutManager(this,4));
        picRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        picModelAdapter = new InstaPicAdapter(getApplicationContext(),arrPicModels,itemOnClick);
        picRecyclerView.setAdapter(picModelAdapter);
        final ParseQuery<ParseObject> queryImgs = ParseQuery.getQuery("InstaImages");
        queryImgs.whereEqualTo("username",userPosts);
        queryImgs.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> userPosts, ParseException e) {
                if (e == null && userPosts.size()>0) {
                    for (int i=0;i<userPosts.size();i++) {
                        try {
                            ParseFile parseFile = userPosts.get(i).getParseFile("img");
                            arrPicModels.add(new PicModel(StaticUtility.ByteArr2Bitmap(parseFile.getData())));
                            picModelAdapter.notifyDataSetChanged();
                        } catch (ParseException parseException) {
                            parseException.printStackTrace();
                        }
                    }
                    ParseObject.pinAllInBackground("PinPics",userPosts);
                } else {
//                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        });
    }
    final InstaPicAdapter.ItemOnClick itemOnClick = new InstaPicAdapter.ItemOnClick() {
        @Override
        public void onItemClick(View view) { //}, int position) {
            Intent picCardView = new Intent(context,PicCardView.class);
            picCardView.putExtra("ItemNo",picRecyclerView.getChildAdapterPosition(view));
            picCardView.putExtra("UserName",userPosts);
            startActivity(picCardView);

        }
    };
    @Override
    public void onBackPressed() {
        try {
            ParseClass.deleteAll("PinPics");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        finish();
        return;
    }
}